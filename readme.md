## Install rust toolchain
```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

rustup toolchain install stable
```

## Compile release
in project working directory.
```bash
cargo build --release 
```
output in `target/release`.