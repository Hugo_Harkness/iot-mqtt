//author: Hugo Tritz

use std::slice;

#[derive(Debug)]
pub enum DataFrame {
    Measurement1{ temp: u16, hum: u8, light: u32, uv: u8, w_speed: u16 },
    Measurement2{ w_dir: u16, rain_int: u32, pres: u16},
    Battery3{ batt: u16 },
    BatterySoft4{ batt: u16 , ver: u32, mui: u16, gui: u16 },
    Conn5{ mui: u16, gui: u16 },
    Error6{ err: u16 }
}

pub fn parse_data (data: Vec<u8>) -> Vec<DataFrame> {

    let mut data_iter = data.iter();

    let mut res = vec![];

    while let Some(df) = parse_frame(&mut data_iter) {
        res.push(df)
    }

    res
}

fn parse_frame (data: &mut slice::Iter<u8>) -> Option<DataFrame> {

    match data.next()? {
        1 => Some(DataFrame::Measurement1 {
                temp: get_u16(data)?,
                hum: get_u8(data)?,
                light: get_u32(data)?,
                uv: get_u8(data)?,
                w_speed: get_u16(data)?,
            }),
        2 => Some(DataFrame::Measurement2 {
            w_dir: get_u16(data)?,
            rain_int: get_u32(data)?,
            pres: get_u16(data)?,
        }),
        3 => Some(DataFrame::Battery3 { batt: get_u16(data)? }),
        4 => Some(DataFrame::BatterySoft4 {
            batt: get_u16(data)?,
            ver: get_u32(data)?,
            mui: get_u16(data)?,
            gui:get_u16(data)?,
        }),
        5 => Some(DataFrame::Conn5 {
            mui: get_u16(data)?,
            gui:get_u16(data)?,
        }),
        6 => Some(DataFrame::Error6 { err: get_u16(data)? }),

        &_ => None
    }
}

fn get_u8 (data: &mut slice::Iter<u8>) -> Option<u8> {
    Some(data.next()?.clone())
}

fn get_u16 (data: &mut slice::Iter<u8>) -> Option<u16> {
    Some(((data.next()?.clone() as u16) << 8) | data.next()?.clone() as u16)
}

fn get_u32 (data: &mut slice::Iter<u8>) -> Option<u32> {
    Some(((data.next()?.clone() as u32) << 24) | ((data.next()?.clone() as u32) << 16) | ((data.next()?.clone() as u32) << 8) | data.next()?.clone() as u32)
}