//author: Hugo Tritz

mod payload_parser;
mod config;

use std::time::{Duration, SystemTime};
use base64::Engine;
use base64::prelude::BASE64_STANDARD;
use chrono::{DateTime, Utc};
use rumqttc::{AsyncClient, Event, Incoming, MqttOptions, QoS};
use serde::{Deserialize, Serialize};
use serde_json::{Number};
use crate::config::config;
use crate::payload_parser::DataFrame;

#[tokio::main]
async fn main() {

    let system_time = SystemTime::now();
    let datetime: DateTime<Utc> = system_time.into();

    let output_file_path = format!("{}-{}.csv", config().output_file_path_without_extension.clone(), datetime.format("%+"));

    let mut wr = csv::Writer::from_path(output_file_path).unwrap();

    let mut mqttoptions = MqttOptions::new("rumqtt-async", config().mqtt_host.clone(), config().mqtt_port.clone());
    mqttoptions.set_keep_alive(Duration::from_secs(5));
    mqttoptions.set_credentials(config().mqtt_username.clone(), config().mqtt_password.clone());

    let (client, mut eventloop) = AsyncClient::new(mqttoptions, 10);
    client.subscribe(config().mqtt_topic.clone(), QoS::AtMostOnce).await.unwrap();

    while let Ok(notification) = eventloop.poll().await {
       // println!("Received = {:?}", notification);

        let Event::Incoming(incomming) = notification else {
            continue
        };

        let Incoming::Publish(publish) = incomming else {
            continue
        };

        if publish.topic.eq(&config().mqtt_topic) {
            let msg: MqttMsg = serde_json::from_slice(publish.payload.iter().as_slice()).unwrap_or_else(|e| {
                println!("{:#?}", publish.payload);
                panic!("{:#?}", e);
            });

            let data = BASE64_STANDARD.decode(msg.data).expect("expected Base64");

            let payload = payload_parser::parse_data(data);

            let record: Record = (payload, msg.time).into();

            println!("{:?}", record);

            wr.serialize(record).unwrap();
            wr.flush().unwrap();

        }
    }

}

#[derive(Deserialize)]
struct MqttMsg {
    data: String,
    time: String
}

#[derive(Serialize, Debug, Default)]
#[allow(non_snake_case)]
struct Record {
    timestamp: String,
    Temperature: Option<Number>,
    Humidity: Option<Number>,
    Light: Option<Number>,
    UV: Option<Number>,
    Wind_Speed: Option<Number>,
    Wind_Direction: Option<Number>,
    Rain_Intensity: Option<Number>,
    Barometric_Pressure: Option<Number>,
}

impl From<(Vec<DataFrame>, String)> for Record {
    fn from((data_frames, ts): (Vec<DataFrame>, String)) -> Self {
        let mut rec = Record::default();

        rec.timestamp = ts;

        for data_frame in data_frames {
            match data_frame {
                DataFrame::Measurement1 { temp,hum, light, uv, w_speed } => {
                    rec.Temperature = Some(Number::from(temp));
                    rec.Humidity = Some(Number::from(hum));
                    rec.Light = Some(Number::from(light));
                    rec.UV = Some(Number::from(uv));
                    rec.Wind_Speed = Some(Number::from(w_speed));
                }
                DataFrame::Measurement2 { w_dir, rain_int, pres } => {
                    rec.Wind_Direction = Some(Number::from(w_dir));
                    rec.Rain_Intensity = Some(Number::from(rain_int));
                    rec.Barometric_Pressure = Some(Number::from(pres));
                }
                DataFrame::Battery3 { .. } => {}
                DataFrame::BatterySoft4 { .. } => {}
                DataFrame::Conn5 { .. } => {}
                DataFrame::Error6 { .. } => {}
            }
        }

        rec
    }
}