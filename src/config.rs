//author: Hugo Tritz

use std::fs;
use std::sync::OnceLock;
use serde::{Deserialize, Serialize};

pub const CONFIG_PATH: &'static str = "./config.json";

pub fn config() -> &'static Config {
    static INSTANCE: OnceLock<Config> = OnceLock::new();

    INSTANCE.get_or_init(|| {
        Config::load().unwrap_or_else(|err| {
            panic!("FATAL - ERROR WHILE LOADING CONFIG - Cause: {err:?}")
        })
    })
}

#[derive(Deserialize, Serialize)]
pub struct Config {
    pub mqtt_host: String,
    pub mqtt_port: u16,
    pub mqtt_username: String,
    pub mqtt_password: String,
    pub mqtt_topic: String,

    pub output_file_path_without_extension: String,
}

impl Config {
    fn load() -> Result<Self> {

        let config_file = fs::read_to_string(CONFIG_PATH).map_err(|_| Error::UnableToOpenConfigFile(CONFIG_PATH))?;

        Ok(serde_json::from_str(&config_file).map_err(|pe| Error::ConfigFileParseFailed(pe.to_string()))?)
    }
}

pub type Result<T> = core::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    UnableToOpenConfigFile(&'static str),
    ConfigFileParseFailed(String),
}